<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Galeri;

class GaleriController extends Controller
{
    public function galeri()
    {
        $data = galeri::all();
        return view('admin.galeri.galeri', compact('data'));
    }

    public function tambahgaleri()
    {
        return view('admin.galeri.tambahgaleri');
    }

    public function deletgaleri($id)
    {
        $data = galeri::find($id);
        $data->delete();
        return redirect()->route('galeri');
    }

    public function insertgaleri(Request $request)
    {
        // dd($request->all());
        $data = galeri::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('fotogaleri/', $request->file('foto')->getClientoriginalName());
            $data->foto = $request->file('foto')->getClientoriginalName();
            $data->save();
        }
        return redirect()->route('galeri');
    }

    public function updategaleri(Request $request, $id)
    {
        $data = galeri::find($id);
        $data->update($request->all());
        return redirect()->route('galeri');
    }

    public function tampilgaleri($id)
    {
        $data = galeri::find($id);
        // dd($data);
        return view('admin.galeri.tampilgaleri', compact('data'));
    }
}
