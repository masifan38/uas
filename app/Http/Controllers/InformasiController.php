<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\informasi;

class InformasiController extends Controller
{
    public function informasi()
    {
        $data = informasi::all();
        return view('admin.informasi.informasi', compact('data'));
    }

    public function tambahinformasi()
    {
        return view('admin.informasi.tambahinformasi');
    }

    public function insertinformasi(Request $request)
    {
        // dd($request->all());
        $data = informasi::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('fotoinformasi/', $request->file('foto')->getClientoriginalName());
            $data->foto = $request->file('foto')->getClientoriginalName();
            $data->save();
        }
        return redirect()->route('informasi')->with('sukses', 'Data berhasil di tambahkan');
    }

    public function tampilinformasi($id)
    {
        $data = informasi::find($id);
        // dd($data);
        return view('admin.informasi.tampilinformasi', compact('data'));
    }

    public function updateinformasi(Request $request, $id)
    {
        $data = informasi::find($id);
        $data->update($request->all());
        return redirect()->route('informasi');
    }

    public function deletinformasi($id)
    {
        $data = informasi::find($id);
        $data->delete();
        return redirect()->route('informasi');
    }
}
