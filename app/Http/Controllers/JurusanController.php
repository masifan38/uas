<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jurusan;

class JurusanController extends Controller
{
    public function jurusan()
    {
        $data = Jurusan::all();
        return view('admin.jurusan.jurusan', compact('data'));
    }

    public function tambahjurusan()
    {
        return view('admin.jurusan.tambahjurusan');
    }

    public function insertjurusan(Request $request)
    {
        $data = jurusan::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('fotojurusan/', $request->file('foto')->getClientoriginalName());
            $data->foto = $request->file('foto')->getClientoriginalName();
            $data->save();
        }
        return redirect()->route('jurusan');
    }

    public function tampilkanjurusan($id)
    {
        $data = jurusan::find($id);
        // dd($data);
        return view('admin.jurusan.tampilkanjurusan', compact('data'));
    }

    public function updatejurusan(Request $request, $id)
    {
        $data = jurusan::find($id);
        $data->update($request->all());
        return redirect()->route('jurusan');
    }

    public function deletjurusan($id)
    {
        $data = jurusan::find($id);
        $data->delete();
        return redirect()->route('jurusan');
    }
}
