<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jurusan;
use App\Models\Galeri;
use App\Models\Informasi;

class WelcomeController extends Controller
{
    public function index()
    {
        $ju = Jurusan::all();
        $ga = Galeri::all();
        $in = Informasi::all();
        return view('welcome', compact('ju', 'ga', 'in'));
    }
}
