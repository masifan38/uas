<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <title>SMKN Indonesia Raya</title>
    <style>
        body{
            padding-top: 60px;
        }
      .h-100vh{
        background-image: url(assets/img/foto1.jpg);
        background-repeat: no-repeat;
        background-size: cover;
        padding: 100px;
        height: 80vh;
        }

        .kontak{
        background-image: url(assets/img/ak.png);
        background-repeat: no-repeat;
        background-size: cover;
        padding: 100px; 
        }

        .foto {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
            margin-top: 46px;
            }
        
        .font-60{
            font-size: 60px;
        }
        .font-bold{
            font-weight: bold;
        }
        
        .p-100{
            padding: 100px;
        }
        @media (max-width: 768px){} 
            }
            .p-100{
            padding: 100px 0;
            }
    </style>

</head>
<body>
  {{--  navbar --}}
  
  <nav class="navbar container-fluid fixed-top navbar-expand-lg navbar-dark bg-primary">
    <div class="logo">
      <img src="assets/img/logo.png" width="70">
    </div>
    <div class="container-fluid ">
      <a class="navbar-brand " href="/">SMKN Indonesia Raya</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="mx-auto"></div>
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#tentang">Tentang Sekolah</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#jurusan">Jurusan</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#galeri">Galeri</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#informasi">Informasi</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#kontak">Kontak</a>
              </li>
            </ul>
            <ul class="navbar-nav ms-auto">
                <!-- Authentication Links -->

                @guest
                @else
                    <li class="nav-item dropdown mx-5">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <a class="dropdown-item" href="/home">Dashboard</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
            
        </div>
    </div>
  </nav>
{{-- and navbar --}}
{{-- bagian beranda --}}
    <div class=" h-100vh" id="home">
        <div class="container text-center bg-dark text-light pl-0 bg-opacity-75">
            <h3>Selamat Datang di Sekolah Indonesia raya</h3>
            <p>sekolah menengan kejuruan dengan berbagai  jurusan yang dapat anda pilih sesuai minat dan bakat anda.</p>
        </div>
    </div>
    <section class="containet-fluid h-100 bg-light " id="tentang">
            <div class="row-bg-section">
                <div class="p-100">
                    <div class="col-md-20">
                        <h3 class="text-center text-dark">SAMBUTAN KEPALA SEKOLAH</h3>
                        <img class="foto" src="assets/img/aa.png" alt="" style="width:150px;">
                        <h4 class="text-center mt-4 mb-3">INDRA ROFIYADI Spd</h4>
                        <p class="text-center">Sekamat Datang Para Pencari Inspirasi, Semoga Kalian Sehat Semua, Semangat Terus Dalam Menuntut Ilmu, Jangan Pernah Bosan Kelak Ilmu Yang Kamu Pelajari Hari Ini Akan Bermanfaat Dimasa Yang Akan Datang, Trus Semangat Dan Semoga Harimu Bahagia.</p>
        
                    </div>
                </div>
            </div>
        </section>
        <section class="containet-fluid h-100 bg-light " id="tentang">
            <div class="row-bg-section">
                <div class="p-100">
                    <div class="col-md-20">
                        <h3 class="text-center text-dark">SAMBUTAN WAKIL KEPALA SEKOLAH</h3>
                        <img class="foto" src="assets/img/aa.png" alt="" style="width:150px;">
                        <h4 class="text-center mt-4 mb-3">SYAIFUL RIZAL Spd</h4>
                        <p class="text-center">Tetap semangat menjadi siswa berprestasi, Untuk membangun indonesia lebih maju, Menjadi kebanggan sekolah Dan orang tua </p>
        
                    </div>
                </div>
            </div>
        </section>
        <div class="row-bg-section">
            <div class="p-100">
                <div class="col-md-20">
                    <h2 class="text-center">VISI MISI</h2>
                    <h3 class="mb-4 mt-4">VISI</h3>
                    <p>Berprestasi dilandasi Iman, Taqwa dan Berbudaya Lingkungan serta Berwawasan Global</p>
                    <h3 class="mb-4 mt-4">MISI</h3>
                    <p>1.   Mewujudkan pendidikan untuk menghasilkan prestasi dan lulusa berkwalitas tinggi yang peduli dengan lingkungan hidup</p>
                    <p>2.   Mewujudkan sumber daya manusia yang beriman, produktif, kreatif, inofatif dan efektif</p>
                    <p>3.   Mewujudkan pengembangan inovasi pembelajaran sesuai tuntutan</p>
                    <p>4.   Mewujudkan sumber daya manusia yang peduli dalam mencegahan pencemaran, mencegahan kerusakan lingkungan dan melestarikan lingkungan hidup</p>
                    <p>5. Mewujudkan manusia Indonesia yang mampu berkontribusi pada kehidupan bermasyarakat, berbangsa, bernegara dalam peradaban dunia</p>
                </div>
            </div>
        </div>
    </section>
    <section class="containet-fluid h-100 bg-light " id="jurusan">  
        <div class="row bg-section">
            <div class="p-100">
                <div class="col-md-20 ">
                    <h1 class="text-center">jurusan</h1>
                    <div class="row mt-5 pt-5">
                        @foreach ($ju as $row)
                            
                        <div class="col-md-4 mb-3">
                            <div class="card" style="width: 18rem;">
                                <img src="{{ asset('fotojurusan/'.$row->foto) }}" class="card-img-top" alt="">
                                <div class="card-body">
                                <h5 class="card-title">{{ $row->nama }}</h5>
                                <p class="card-text">{{ $row->keterangan }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <section class="containet-fluid h-100 bg-light " id="galeri">  
        <div class="row bg-section">
            <div class="p-100">
                <div class="col-md-20 ">
                    <h1 class="text-center">Galeri</h1>
                    <div class="row mt-5 pt-5">
                        @foreach ($ga as $row)
                            
                        <div class="col-md-4 mb-3">
                            <div class="card" style="width: 18rem;">
                                <img src="{{ asset('fotogaleri/'.$row->foto) }}" class="card-img-top" alt="">
                                <div class="card-body">
                                  <h5 class="card-title"></h5>
                                  <p class="card-text">{{ $row->keterangan }}</p>
                                </div>
                              </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>    
        </div>
    </section>
    <section class="containet-fluid h-100 bg-light " id="informasi">
        <div class="row bg-section">
            <div class="p-100">
                <div class="col-md-20 ">
                    <h1 class="text-center">informasi</h1>
                    <div class="row mt-5 pt-5">
                        @foreach ($in as $row)
                            
                        <div class="col-md-4 mb-3">
                            <div class="card" style="width: 18rem;">
                                <img src="{{ asset('fotoinformasi/'.$row->foto) }}" class="card-img-top" alt="">
                                <div class="card-body">
                                  <h5 class="card-title"></h5>
                                  <p class="card-text">{{ $row->keterangan }}</p>
                                </div>
                              </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>    
        </div>  
    </section>

    <section class="kontak container-fluid"id="kontak">
        <div class="row" id="kontak">
            <div class="col-md-12">
              <div class="p-100">
                <h1 class="mb-5 text-light">Kontak</h1>
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3 font-20">
                            <i class="fa fa-map text-info"></i>
                            <small class="px-3 text-light">
                                Jl.indonesia raya
                            </small>
                        </div>
    
                        <div class="mb-3 font-20">
                            <i class="fa fa-phone text-light"></i>
                            <small class="px-3 text-light">
                                +62 87777777777
                            </small>
                        </div>
    
                        <div class="mb-3 font-20">
                            <i class="fa fa-envelope text-info"></i>
                            <small class="px-3 text-light">
                                smkindoraya@gmail.com
                            </small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                           <h3 class="text-center text-light">Kirim Pesan</h3>
                         </div>
                        <div class="mb-3">
                            <input type="text" class="form-control form-control-lg" placeholder="Nama Lengkap">
                        </div>
                        <div class="mb-3">
                            <input type="email" class="form-control form-control-lg" placeholder="Email">
                        </div>
                        <div class="mb-3">
                            <textarea class="form-control form-control-lg" placeholder="Tulis Pesan"></textarea>
                        </div>
                        <div class="mb-3">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="d-grid gap-2">
                                        <button class="btn btn-lg btn-block text-light bg-dark">
                                            <i class="fa fa-paper-plane"></i> Submit
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
        </section>
<div class="pixed-bottom">
    <footer class="d-flex  flex-wrap justify-content-between align-items-center py-3 my-4 mt-4 border-top bg-primary">
        <div class="col-md-4 d-flex align-items-center">
            <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
                <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"/></svg>
            </a>
            <span class="text-light">&copy; 2022 SMKN Indonesia Raya</span>
        </div>
    </footer>
    <script src="assets/js/bootstrap.bundle.js"></script>
</div>

</body>
</html>