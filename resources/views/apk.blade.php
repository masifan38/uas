
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <style>
      body{
        padding-top: 70px;
      }
      .h-100vh{
        background-image: url(assets/img/foto1.jpg);
        background-repeat: no-repeat;
        background-size: cover;
        padding: 100px;
        height: 80vh;
        }

      .Container{
        padding: 100px;
      }
    </style>
    <title>Dashboard </title>


  </head>
  <body >

    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary bg-opacity-75 mb-4">
      <div class="logo">
        <img src="assets/img/logo.png" width="70">
      </div>
      <div class="container-fluid">
        <a class="navbar-brand" href="/">SMKN indonesia Raya</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ms-auto mb-lg-0">
            <li class="nav-item mx-2">
              <a class="nav-link" aria-current="page" href="/home">Dashboard</a>
            </li>
            <li class="nav-item mx-2">
              <a class="nav-link" aria-current="page" href="/user">User</a>
            </li>
            <li class="nav-item mx-2">
              <a class="nav-link" aria-current="page" href="/jurusan">Jurusan</a>
            </li>
            <li class="nav-item mx-2">
              <a class="nav-link" aria-current="page" href="/galeri">Galeri</a>
            </li>
            <li class="nav-item mx-2">
              <a class="nav-link" aria-current="page" href="/informasi">Informasi</a>
            </li>
            @guest
            @else
            <li class="nav-item dropdown mx-5">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                </a>
      
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
      
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
            @endguest
          </ul>
        </div>
      </div>

    </nav>
    {{-- and nafbar --}}
    
<section class="Container pt-4">
    @yield('name')
</section>

<footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 mt-4 border-top bg-primary">
  <div class="col-md-4 d-flex align-items-center">
    <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
      <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"/></svg>
    </a>
    <span class="text-light">&copy; 2022 SMKN Indonesia Raya</span>
  </div>
</footer>
<script src="assets/js/bootstrap.bundle.js"></script>
</body>
</html>
