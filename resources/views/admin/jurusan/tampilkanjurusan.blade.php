@extends('apk')

@section('name')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <form method="POST" action="/updatejurusan/{{ $data->id }}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Nama</label>
                  <input type="text" name="nama" class="form-control"  aria-describedby="emailHelp" value="{{ $data->nama }}">
                </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Keterangan</label>
                  <input type="text" name="keterangan" class="form-control"  aria-describedby="emailHelp" value="{{ $data->keterangan }}">
                </div>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Pilih Foto</label>
                  <input type="file" name="foto" class="form-control" value="{{ $data->foto }}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
</div>

