@extends('apk')

@section('name')
<h1 class="text-center mb-4">Galeri</h1>
<div class="container">
    <a href="/tambahgaleri" class="btn btn-success mb-3">Tambah +</a>
<table class="table table-success table-striped">
<thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">keterangan</th>
      <th scope="col">foto</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
     @foreach ($data as $row)
       <tr>
        <th scope="row">{{ $row->id }}</th>
        <td class="text-dark">{{ $row->keterangan }}</td>
        <td class="text-dark">
            <img src="{{ asset('fotogaleri/'.$row->foto) }}" alt="" style="width: 100px;">
        </td>
        <td>
          <a href="/deletgaleri/{{ $row->id }}" class="btn btn-danger mb-2">Delete</a>
          <a href="/tampilgaleri/{{ $row->id }}" class="btn btn-info mb-2">Edit</a>
        </td>
      </tr>
    @endforeach
    

    
  </tbody>
</table>
</div>