@extends('apk')

@section('name')
<h1 class="text-center mb-4 mt-3">Data User</h1>
<div class="container">
<table class="table table-success table-striped">
<thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nama</th>
      <th scope="col">Email</th>
      <th scope="col">Pasword</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($data as $row)
    <tr>
        <th scope="row">{{ $row->id }}</th>
        <td class="text-dark">{{ $row->name }}</td>
        <td class="text-dark">{{ $row->email }}</td>
        <td class="text-dark"></td>
        <td>
          <a href="deletuser/{{ $row->id }}" class="btn btn-danger">Delete</a>
        </td>
      </tr>
    @endforeach
    
  </tbody>
</table>
</div>