<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\WelcomeController::class, 'index'])->name('welcome');

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/user', [App\Http\Controllers\HomeController::class, 'user'])->name('user');

    Route::get('deletuser/{id}', [App\Http\Controllers\HomeController::class, 'deletuser'])->name('deletuser');

    Route::get('/jurusan', [App\Http\Controllers\JurusanController::class, 'jurusan'])->name('jurusan',);
    Route::get('/tambahjurusan', [App\Http\Controllers\JurusanController::class, 'tambahjurusan'])->name('tambahjurusan');
    Route::post('/insertjurusan', [App\Http\Controllers\JurusanController::class, 'insertjurusan'])->name('insertjurusan',);
    Route::get('deletjurusan/{id}', [App\Http\Controllers\JurusanController::class, 'deletjurusan'])->name('deletjurusan');
    Route::get('tampilkanjurusan/{id}', [App\Http\Controllers\JurusanController::class, 'tampilkanjurusan'])->name('tampilkanjurusan');
    Route::post('updatejurusan/{id}', [App\Http\Controllers\JurusanController::class, 'updatejurusan'])->name('updatejurusan');

    Route::get('/galeri', [App\Http\Controllers\GaleriController::class, 'galeri'])->name('galeri');
    Route::get('/tambahgaleri', [App\Http\Controllers\GaleriController::class, 'tambahgaleri'])->name('tambahgaleri');
    Route::post('/insertgaleri', [App\Http\Controllers\GaleriController::class, 'insertgaleri'])->name('insertgaleri');
    Route::get('/deletgaleri/{id}', [App\Http\Controllers\GaleriController::class, 'deletgaleri'])->name('deletgaleri');
    Route::get('/tampilgaleri/{id}', [App\Http\Controllers\GaleriController::class, 'tampilgaleri'])->name('tampilgaleri');
    Route::post('/updategaleri/{id}', [App\Http\Controllers\GaleriController::class, 'updategaleri'])->name('updategaleri');

    Route::get('/informasi', [App\Http\Controllers\InformasiController::class, 'informasi'])->name('informasi');
    Route::get('/tambahinformasi', [App\Http\Controllers\InformasiController::class, 'tambahinformasi'])->name('tambahinformasi');
    Route::post('/insertinformasi', [App\Http\Controllers\InformasiController::class, 'insertinformasi'])->name('insertinformasi');
    Route::get('/deletinformasi/{id}', [App\Http\Controllers\InformasiController::class, 'deletinformasi'])->name('deletinformasi');
    Route::get('/tampilinformasi/{id}', [App\Http\Controllers\InformasiController::class, 'tampilinformasi'])->name('tampilinformasi');
    Route::post('/updateinformasi/{id}', [App\Http\Controllers\InformasiController::class, 'updateinformasi'])->name('updateinformasi');
});
